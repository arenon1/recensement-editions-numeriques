**Objectif du travail **:
Réaliser un  inventaire des projets d’éditions numériques de documents lexicographiques et textes antiques (achevés ou en cours) et rédaction des notices correspondantes dans le logiciel Scenari, en français et en anglais

**Atelier Scenari Idkey 2** : "Notice_inventaire_lexico"
Il est possible d'importer directement les fichiers contenus dans "sources"

**Fichier "Inventaire_projets.idkey"**
 Clé de détermination qui permet d'assembler les critères, les entrées de taxonomie : les nœuds de taxons, d'éventuels images, de bloc et les mentions légales dans la page d'accueil. 

**Dossier "criteres"**
fichier .criterion 
- Contient tout les critères et leurs valeurs rattachés aux taxons. Les critères s'affichent sur la page d'accueil, nous pouvons les cocher pour trouver un projet d'édition numérique (taxons).
- Chaque fichier est nommé d'après le premier mot de son identifiant (ex. "Opérations", pour "opérations réalisées et outils numériques associés").
 
**Dossier "res" **
Un dossier pour ranger les ressources (images, vidéos, audio, etc). 

**Dossier "taxons"**
fichier .taxon 
Contient les notices de chaque projet 
- Identifiants : Identifiant du projet (Se trouve en haut de la notice, exemple : "lien vers l'ouvrage numérisé", "institution", "porteur du projet", etc)
- Présentation : Une courte présentation de 4 lignes du projet, elle se trouve dans la classification au niveau du projet
- Identification : Les valeurs des critères rattachées au projet (se trouve dans la case identification sur la notice
-Description : Bloc de test servant à décrire le contenu, l'analyse et la valorisation du projet.
Il est aussi possible d'ajouter une licence et des illustrations
Chaque fichier est nommé d'après le titre abrégé du projet.

**Dossier "taxonomie"**
fichier .node
Contient les noeuds de taxonomie servant à réaliser un classement des projet (par exemple : "Editions numérique" > "Textes lexicographiques" > "Lexicographie générale" > "Lexicographie monolingue" > "Nénufar".
Les nœuds sont rassemblés dans un sous dossier (ANT pour antiquité et LEX pour lexicographie) 
Les nœuds sont consultable en cliquant sur l'onglet parcourir la taxonomie et dans la classification du projet présent sur la notice.
Le dossier "LEX" est divisé en deux sous-dossiers intitulés : "1_langue_generale" et "2_langue_specialite". Le numéro permet ensuite d'associer les noeuds afin de ne pas répéter par exemple "Lexicographie générale" dans les noeuds "monolingue, bilingue, multilingue".
Il est possible d'ajouter une licence, une présentation et des illustrations

**Dossier "critère_non_utilisés"**
Contient les critères non retenus 




