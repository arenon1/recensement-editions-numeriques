 idKeyMgr = scOnLoads[scOnLoads.length] = {
	datasUrl : null,
	datas : {},			//data chargées depuis datas.json
	state : [],			//etat de sélection courant
	reverseCritsState:{},
	critLoadedCb : {},
	strings : {
		valid : "Confirmer",
		cancel : "Annuler",
		select : "Sélectionner",
		close : "Fermer",
		open : "Ouvrir",
		edit : "Éditer",
		remove : "Supprimer",
		return : "Retour",
		choose : "Choisir",
		reset : "Réinitialiser",
		criterions : "Critères",
		selection : "Sélection",
		share : "Partager la sélection",
		critsOn : " critères respectés sur ",
		critOn : " critère respecté sur ",
		returnToCrits : "Retour aux critères",
		error : "Une erreur est survenue. Veuillez recharger la page pour recommencer votre détermination de taxon",
		seeMore : "Voir plus de critères",
		seeMoreTitle : "Voir les critères qui ne concernent qu\'une partie des taxons restant",
		seeLessTitle : "Voir uniquement les critères principaux",
		seeLess : "Voir moins",
		details : "Détails",
		seeDetails : "Ouvrir les détails de cette valeur de critère",
		closeDetails : "Fermer les détails de cette valeur de critère",
		filter : "Recherche",
		applyFilter : "Rechercher",
		next : "Suivant",
		previous :"Précédent",
		info : "En savoir plus ?",
		legal : "Mentions légales",
		selecteds : " taxons sélectionnés",
		selected : " taxon sélectionné",
		selectedOn : " taxon sélectionné sur ",
		selectedsOn : " taxons sélectionnés sur ",
		loading : "Chargement en cours...",
		installApp : "Installer l\'application",
		prepareOffline : "Télécharger pour usage offline",
		prepareUpdate : "Mettre à jour l\'application",
		updateOfflineReady : "<div><span>Une mise à jour est disponible.</span></div>",
		offlineWarning : "<div class='warning'><span>Attention, le poids de l\'application est estimé à %s. Le téléchargement peut entrainer des surcoûts en fonction de la nature de votre accès internet.</span></div><div><span>Souhaitez-vous poursuivre maintenant ?</span></div>",
		updateWarning : "<div class='warning'><span>Attention, le volume de données à télécharger est estimé à %s. Cela peut entrainer des surcoûts en fonction de la nature de votre accès internet.</span></div><div><span>Souhaitez-vous poursuivre maintenant ?</span></div>",
		offlineLoadingStarted : "Le téléchargement est en cours, vous pouvez continuer votre navigation.",
		offlineReady : "L\'application est prête à fonctionner offline.",
		updateReady : "Une mise à jour est disponible, la page doit être rechargée.",
		notEnoughSpace :"Le navigateur ne dispose pas d\'assez de place pour installer l\'application (%s disponible sur %s requis)",
		offlineFailed : "Une erreur a eu lieu dans le téléchargement. Vous ne pouvez pas utiliser l\'application offline.",
		featureNotAvailable : "Impossible de télécharger, votre navigateur refuse d\'allouer un espace de stockage. Vérifier les autorisations du navigateur.",
		downloading : "Téléchargement en cours",
		downloaded : "Application offline",
		noMoreCrits : "Aucun critère supplémentaire disponible",
		closeHisto : "Fermer l\'historique",
		openHisto : "Ouvrir l\'historique",
		definedCrits : " critères définis",
		definedCrit : " critère défini",
		copySuccess : "URL copiée dans le presse-papier",
		placeHolderKeys : "Rechercher un critère",
		placeHolderTxns : "Rechercher un taxon",
		selectNewCriterion : "Sélectionner un nouveau critère",
		selectTitle : "Valider la sélection de ce taxon",
		addMyPhoto : "Ajouter ma photo"
	},

	// active/désactive le pushState dans l'histo
	inReloadUrlState : false,
	//active/désactive le bouton de sélection CID d'un taxon
	cid:false,
	/**
	 * Calcul et affichage de l'historique et du menu de sélection d'un prochain critère
	 */
	nextSelStep : function(){
		const bd = dom.newBd(sc$("keys")).clear();
		bd.elt("div", "keysHead")
			.elt("span", "keysTitle").text(idKeyMgr.strings.criterions).up();
			if(idKeyMgr.state.length) bd.elt("button").att("title", idKeyMgr.strings.share).listen("click", idKeyMgr.onShare).elt("span").text(idKeyMgr.strings.share).up().up();
			bd.elt("div")
				.elt("input", "addPhoto desktop").att("type", "file").att("title", idKeyMgr.strings.addMyPhoto).att("accept", "image/*").listen("change", idKeyMgr.onPhotoAdded).up()
				.elt("label").att("title", idKeyMgr.strings.addMyPhoto).att("for", "addMyPhotoInput").elt("span").text(idKeyMgr.strings.addMyPhoto).up().up()
			.up()
		bd.up();

		bd.elt("div", "keys");
		idKeyMgr.xBuildHTMLHistoCritLst();
		idKeyMgr.xBuildHTMLTaxonsLst();

		//Reprise sur state incomplet, modifié ou supprimé.
		for(let i = 0 ; i < idKeyMgr.state.length ; i++){
			if(i === 0) {
				if (idKeyMgr.datas.exclusiveCrits?.indexOf(idKeyMgr.state[0].crit) === -1) {
					idKeyMgr.xBuildHTMLExclusiveCrits(idKeyMgr.datas.exclusiveCrits, "noReturn");
					return;
				}
			}
			else {
				const exclusiveCrits = [];
				idKeyMgr.state[i - 1].critVals.forEach(critValName =>{
					const critVal = idKeyMgr.getCritVal(idKeyMgr.state[i - 1].crit, critValName);
					if (critVal.exclusiveCrits?.indexOf(idKeyMgr.state[i].crit) === -1) {
						critVal.exclusiveCrits?.forEach(exCrit =>{
							if(idKeyMgr.getHistoCrit(exCrit) === null) exclusiveCrits.push(exCrit);
						});
					}
				});
				if(exclusiveCrits.length){
					idKeyMgr.xBuildHTMLExclusiveCrits(exclusiveCrits, "noReturn");
					return;
				}
			}

		}

		//Next step
		const lastStep = idKeyMgr.state.length ? idKeyMgr.state[idKeyMgr.state.length-1] : null;
		const exclusiveCrits = [];
		if(lastStep !== null) lastStep.critVals.forEach(critValNam =>{
			const critVal = idKeyMgr.getCritVal(lastStep.crit, critValNam);
			critVal?.exclusiveCrits?.forEach(exCrit => {
				if(idKeyMgr.getHistoCrit(exCrit) === null) exclusiveCrits.push(exCrit);
			});
		});

		//exclusive crit
		if(!idKeyMgr.state.length && idKeyMgr.datas.exclusiveCrits) idKeyMgr.xBuildHTMLExclusiveCrits(idKeyMgr.datas.exclusiveCrits, "noReturn");
		else if(exclusiveCrits.length) idKeyMgr.xBuildHTMLExclusiveCrits(exclusiveCrits);
		else{
			//seconday crit
			idKeyMgr.xBuildHTMLNextCritLst();
		}
		idKeyUi.xHistoHtmlChange();
	},

	getCritName : function(param){
		if(typeof param === "string") return param;
		else return (scPaLib.findNode("can:ul.critVals",param)||scPaLib.findNode("can:div.step",param)||scPaLib.findNode("can:div.critSlider",param)).getAttribute("data-crit");
	},
	getCrit : function(param){
		return this.datas.crits[this.getCritName(param)];
	},
	getCritValName : function(param){
		if(typeof param === "string") return param;
		else return (scPaLib.findNode("can:li.critVal",param)||scPaLib.findNode("can:div.step",param)).getAttribute("data-critVal");
	},
	getCritVal : function(param, critValName){
		const crit = this.getCrit(param);
		if(! crit) return null;
		else if(crit.type === "slider") {
			if(critValName && crit.critvals[critValName]) return crit.critvals[critValName];
			else {
				const matchedTaxons = {};
				Object.keys(crit.critvals).forEach(k=>{
					const critval = crit.critvals[k];
					if(critValName>=critval.min && critValName <= critval.max) critval.reverseTaxons.forEach(taxon=> matchedTaxons[taxon] = true);
				});
				return {"value":`${critValName} ${crit.unit}`, "reverseTaxons":Object.keys(matchedTaxons)};
			}
		}
		else {
			if(critValName) return crit.critvals[critValName];
			else return crit.critvals[this.getCritValName(param)];
		}
	},
	loadCritDatas : function(critName, cb){
		try{
			if(cb) idKeyMgr.addCritDatasLoadedCb(critName,cb);
			const crit = idKeyMgr.getCrit(critName);
			if(crit.loaded === true) {
				idKeyMgr.onCritDatasLoaded(critName);
				return false;
			}
			else{
				idKeyMgr.fetchJson(crit.descUrl, function(datas){
					crit.shortDesc = datas.shortDesc;
					for(let key in datas.critvals) for(let critValKey in datas.critvals[key]) crit.critvals[key][critValKey] = datas.critvals[key][critValKey];
					crit.loaded = true;
					idKeyMgr.onCritDatasLoaded(critName);
					for(let critVal in crit.critvals) crit.critvals[critVal]?.exclusiveCrits?.forEach(function (critName) {idKeyMgr.loadCritDatas(critName);});
				});
				return true;
			}
		}
		catch (e) {
			idKeyUi.alert(idKeyMgr.strings.error);
			console.error(e);
		}
	},
	getHistoCrit : function(critName){
		for(let i = 0 ; i< idKeyMgr.state.length ; i++) if(idKeyMgr.state[i].crit === critName) return idKeyMgr.state[i];
		return null;
	},
	getHistoCritIndex : function(critName){
		for(let i = 0 ; i< idKeyMgr.state.length ; i++) if(idKeyMgr.state[i].crit === critName) return i;
		return -1;
	},

	onCritDatasLoaded : function(critName){
		if(idKeyMgr.critLoadedCb[critName]) idKeyMgr.critLoadedCb[critName].forEach(cb=>cb.call(idKeyMgr,critName));
		delete idKeyMgr.critLoadedCb[critName];
	},
	addCritDatasLoadedCb : function(critName,cb){
		if(!idKeyMgr.critLoadedCb[critName]) idKeyMgr.critLoadedCb[critName] = [];
		idKeyMgr.critLoadedCb[critName].push(cb);
	},

	/**
	 * Ouvre une valeur de critère suite à clic sur lien.
	 */
	onOpenCritVal : function(event){
		const openBtn = scPaLib.findNode("can:button", event.target)
		const critVal = idKeyMgr.getCritVal(openBtn);
		const li = scPaLib.findNode("can:li", openBtn);

		if(openBtn.classList.contains("seeDetails")){
			const taxons = scPaLib.findNode("can:div.crit", li).taxonsToMatch;
			let score = 0;
			critVal.reverseTaxons.forEach(taxon=>{if(taxons.indexOf(taxon) !== -1) score++;});

			openBtn.classList.remove("seeDetails");
			openBtn.classList.add("closeDetails");
			openBtn.setAttribute("title", idKeyMgr.strings.closeDetails);
			//li.classList.add("selected");
			if(idKeyMgr.getCrit(openBtn).type === "mcq"){
				scPaLib.findNode("des:button.select",sc$("keys")).disabled = false;
			}
			if(taxons.length){
				li.insertAdjacentHTML("beforeend", "<span class='critValScore'>"+score + (score > 1 ? idKeyMgr.strings.selectedsOn : idKeyMgr.strings.selectedOn)+taxons.length+"</span>");
			}
			const bd = dom.newBd(li);
			bd.elt("div", "desc").current().insertAdjacentHTML("beforeend", critVal.desc);
			idKeyMgr.resetMediaMgrs();
		}
		else{
			openBtn.classList.remove("closeDetails");
			openBtn.classList.add("seeDetails");
			openBtn.setAttribute("title", idKeyMgr.strings.seeDetails);
			//li.classList.remove("selected");
			if(idKeyMgr.getCrit(openBtn).type === "mcq"){
				if(scPaLib.findNode("des:li.selected", sc$("keys"))) scPaLib.findNode("des:button.select",sc$("keys")).disabled = false;
				else scPaLib.findNode("des:button.select",sc$("keys")).disabled = true;
			}
			const desc = scPaLib.findNode("des:div.desc",li);
			if(desc) desc.parentElement.removeChild(desc);
			const score = scPaLib.findNode("des:span.critValScore",li);
			if(score) score.parentElement.removeChild(score);
			idKeyMgr.resetMediaMgrs();
		}
	},

	onCritSelected : function(event){
		idKeyUi.xClose();
		const list = scPaLib.findNode("des:div.critlist", sc$("keys"));
		const listParent = list.parentElement;
		listParent.removeChild(list);
		listParent.appendChild(idKeyMgr.xBuildHTMLCrit(idKeyMgr.getCritName(event.target)));
		idKeyMgr.xBrowserHisto("pushState");
	},

	onCritValSelected : function(event){
		const li = scPaLib.findNode("can:li.critVal", event.target);
		const critName = idKeyMgr.getCritName(li);
		const critValName =idKeyMgr.getCritValName(li);
		const crit = idKeyMgr.getCrit(critName);
		if(crit.type === "mcq"){
			li.classList.toggle("selected");
			if(scPaLib.findNode("des:li.selected", sc$("keys"))) scPaLib.findNode("des:button.select",sc$("keys")).disabled = false;
			else scPaLib.findNode("des:button.select",sc$("keys")).disabled = true;
			return;
		}
		idKeyMgr.xValidCritValSelection(critName, [critValName]);
	},
	onClickCritValMulti : function(e){
		if(scPaLib.findNode("can:button", e.target)) return;
		scPaLib.findNode("can:li", e.target).classList.toggle("selected");
		const btn = scPaLib.findNode("des:button.select", sc$("keys"));
		btn.disabled = scPaLib.findNodes("des:li.selected", sc$("keys")).length ? false : true;
	},
	onCritValMultiSelected : function(e){
		const val = [];
		let critName = null;
		scPaLib.findNodes("des:li.selected", sc$("keys")).forEach(function (li){
			if(!critName) critName = idKeyMgr.getCritName(li);
			val.push(idKeyMgr.getCritValName(li));
		});
		idKeyMgr.xValidCritValSelection(critName, val);
	},
	onCritContinuousSelected : function(e){
		const input = scPaLib.findNode("can:button/psi:input", e.target);
		const critName = idKeyMgr.getCritName(input);
		idKeyMgr.xValidCritValSelection(critName, [input.value]);

	},
	onEditHistory : function(event){
		idKeyUi.xClose();
		const critName = idKeyMgr.getCritName(event.target);
		let critVals = [];
		for(let i = 0 ; i < idKeyMgr.state.length ; i++){
			if(critName === idKeyMgr.state[i].crit){
				critVals = idKeyMgr.state[i].critVals;
			}
		}

		const bd = dom.newBd(sc$("keys"));
		bd.clear();
		bd.current().append(idKeyMgr.xBuildHTMLCrit(idKeyMgr.getCritName(event.target), null, critVals));
		idKeyMgr.xBrowserHisto("pushState");
	},

	onRemoveHistory : function(event){
		const critName = idKeyMgr.getCritName(event.target);
		let removedIndex;
		for(let i = 0 ; i < idKeyMgr.state.length ; i++){
			if(idKeyMgr.state[i].crit === critName){
				removedIndex = i;
				idKeyMgr.state.splice(i,1);
				idKeyMgr.xCleanHisto(removedIndex);
				idKeyMgr.xBrowserHisto("pushState");
				idKeyMgr.nextSelStep();
				return;
			}
		}
	},
	onSeeMore : function(event){
		const button = scPaLib.findNode("can:button", event.target);
		const bd = dom.newBd(button.parentElement);
		scPaLib.findNode("des:div.secondaryCrits", sc$("keys")).removeAttribute("hidden")
		button.parentElement.removeChild(button);
		bd.elt("button", "seeLess").att("title", idKeyMgr.strings.seeLessTitle).listen("click",idKeyMgr.onSeeLess).elt("span").text(idKeyMgr.strings.seeLess).up().up();
	},
	onSeeLess : function(event){
		const button = scPaLib.findNode("can:button", event.target);
		const bd = dom.newBd(button.parentElement);
		scPaLib.findNode("des:div.secondaryCrits", sc$("keys")).setAttribute("hidden", "true");
		button.parentElement.removeChild(button);
		bd.elt("button", "seeMore").att("title", idKeyMgr.strings.seeMoreTitle).listen("click",idKeyMgr.onSeeMore).elt("span").text(idKeyMgr.strings.seeMore).up().up();
	},
	onFilter : function(event){
		const input = scPaLib.findNode("anc:label/des:input", event.target);
		if(input.getAttribute("data-filter") === "keys"){
			scPaLib.findNodes("des:.critBlck",sc$("keys")).forEach(node=>{
				if(!input.value) node.removeAttribute("hidden")
				else if(scPaLib.findNode("des:span.crit",node).textContent.toLowerCase().indexOf(input.value.toLowerCase()) !== -1) node.removeAttribute("hidden");
				else node.setAttribute("hidden","true");
			});
		}
		else{
			scPaLib.findNodes("des:li",sc$("taxlist")).forEach(node=>{
				if(!input.value) node.removeAttribute("hidden")
				else if(scPaLib.findNode("chi:a/chi:span",node).textContent.toLowerCase().indexOf(input.value.toLowerCase()) !== -1) node.removeAttribute("hidden");
				else node.setAttribute("hidden","true");
			});
		}
	},

	onOpenTaxo : function(event){
		const input = scPaLib.findNode("can:button",event.target);
		input.setAttribute("onclick","idKeyMgr.onCloseTaxo(event);");
		input.setAttribute("title", idKeyMgr.strings.close);
		input.classList.remove("openTaxo");
		input.classList.add("closeTaxo");
		dom.newBd(input).clear().elt("span").text(idKeyMgr.strings.close);
		scPaLib.findNode("nsi:ul", input).removeAttribute("hidden");
	},
	onCloseTaxo : function(event){
		const input = scPaLib.findNode("can:button",event.target);
		input.setAttribute("onclick","idKeyMgr.onOpenTaxo(event);");
		input.setAttribute("title", idKeyMgr.strings.open);
		input.classList.remove("closeTaxo");
		input.classList.add("openTaxo");
		dom.newBd(input).clear().elt("span").text(idKeyMgr.strings.open);
		scPaLib.findNode("nsi:ul", input).setAttribute("hidden", "true");
	},
	onPhotoAdded : function(event){
		const input = scPaLib.findNode("can:input", event.target);
		if(input.files.length){
				idKeyMgr.onRemovePhoto();
				const reader = new FileReader();
				reader.onload = function (e) {
					sc$("taxlist").insertAdjacentElement("beforebegin", dom.newBd(document.createDocumentFragment()).elt("div").att("id", "ownPhoto")
						.elt("img").att("src", e.target.result).up()
					.elt("div", "tools")
						.elt("button", "remove").att("title", idKeyMgr.strings.remove).listen("click",idKeyMgr.onRemovePhoto).elt("span").text(idKeyMgr.strings.remove).up().up()
					.up().current());
				};
				reader.readAsDataURL(input.files[0]);
		}
		document.body.classList.add("hasPhoto");
	},
	onRemovePhoto : function(event){
		const div = sc$("ownPhoto");
		if(div) div.parentElement.removeChild(div);
		//on ne reset que sur clic du bouton delete, pas sur clean depuis onPhotoAdded
		if(event) scPaLib.findNode("des:input.addPhoto", sc$("navMenu")).value = null;
		document.body.classList.remove("hasPhoto");
	},
	onOpenCloseHisto : function(event){
		const btn = scPaLib.findNode("can:button", event.target);
		const histo = scPaLib.findNode("anc:div.history", btn);
		const isClosed = histo.classList.contains("closed");
		sessionStorage.setItem("histoClosed", isClosed ? "false" : "true");
		btn.setAttribute("title", isClosed ? idKeyMgr.strings.closeHisto : idKeyMgr.strings.openHisto);

		histo.classList.toggle("opened");
		histo.classList.toggle("closed");
		idKeyUi.xHistoHtmlChange()
	},
	onResetHisto : function(event){
		idKeyMgr.state = [];
		idKeyMgr.xBrowserHisto("pushState");
		idKeyMgr.nextSelStep();
	},
	onShare : function(event){
		const bd = dom.newBd(sc$("keys"));
		const input = bd.elt("input").att("id", "urlcopy").prop("value", location.toString()).current();
		input.select();
		input.setSelectionRange(0, 99999); /*mobile*/
		document.execCommand("copy");
		input.parentElement.removeChild(input);
		const btn = scPaLib.findNode("can:button", event.target);
		if(!scPaLib.findNode("nsi:span.copySuccess", btn)){
			bd.setCurrent(btn.parentElement);
			bd.elt("span", "copySuccess").text(idKeyMgr.strings.copySuccess);
		}
	},
	onLoadSubPage : function(page){
		idKeyUi.alert(idKeyMgr.datas[page],null, idKeyMgr.strings[page],true);
	},
	setDatasUrl : function(datasUrl){
		idKeyMgr.datasUrl = datasUrl;
	},
	loadDatas : function(json, errorCtx){
		if(json === null) {
			idKeyUi.alert(idKeyMgr.strings.error);
			console.error(errorCtx);
			return;
		}
		idKeyMgr.datas = json;
		//Loading continuousCritVals
		Object.keys(idKeyMgr.datas.taxons).forEach(taxId=> idKeyMgr.datas.taxons[taxId].critvals.forEach((val, idx)=>{
			if(typeof val === "string") return;
			else{
				const critValKey = btoa(`[${val.min},${val.max}]`);
				if(!idKeyMgr.datas.crits[val.critId]) return;
				const critVal = idKeyMgr.datas.crits[val.critId].critvals[critValKey]||{min:parseFloat(val.min), max:parseFloat(val.max), reverseTaxons:[]};
				critVal.reverseTaxons.push(taxId);
				idKeyMgr.datas.crits[val.critId].critvals[critValKey] = critVal;
				idKeyMgr.datas.taxons[taxId].critvals[idx] = critValKey;
			}
		}));
		idKeyMgr.datas.exclusiveCrits?.forEach(function (critName) {idKeyMgr.loadCritDatas(critName);});
		idKeyMgr.datas.taxonsLst = Object.keys(idKeyMgr.datas.taxons);
		const elision = function(title){
			const str = title.toLowerCase();
			if(str[0] =='l'){
				if(str.startsWith("les ")) return title.substring(4);
				if(str.startsWith("le ")) return title.substring(3);
				if(str.startsWith("la ")) return title.substring(3);
				if(str[1] === '\'') return title.substring(2);
			}
			return title;
		}
		idKeyMgr.datas.taxonsLst.forEach(id=>idKeyMgr.datas.taxons[id].elisedCN = elision(idKeyMgr.datas.taxons[id].commonName));

		const bd = dom.newBd(sc$("footer"));
		if(idKeyMgr.datas.info)
			bd.elt("div", "info")
				.elt("a", "info").att("title", idKeyMgr.strings.info).att("href","#")
					.att("onclick","idKeyMgr.onLoadSubPage('info');return false;").elt("span").text(idKeyMgr.strings.info).up()
				.up()
			.up();
		if(idKeyMgr.datas.legal)
			bd.elt("div", "legal")
				.elt("a", "legal").att("title", idKeyMgr.strings.legal).att("href","#")
					.att("onclick","idKeyMgr.onLoadSubPage('legal');return false;").elt("span").text(idKeyMgr.strings.legal).up()
				.up()
			.up();

		idKeyMgr.xReloadUrlState();
	},
	resetMediaMgrs : function(){
		scPaLib.findNodes("bod:/chi:div.scImgGalCvs").forEach(glr=>glr.parentElement.removeChild(glr));
		scPaLib.findNodes("bod:/des:section.bkGallery/des:a").forEach(a=>{
			a.href = scPaLib.findNode("des:img", a).src;
			a.target = "_blank"
		});
		scImageMgr.onLoad();
		if("tePlayerMgr" in window) tePlayerMgr.init();
		//Gestion des blocks closed
		scPaLib.findNodes("des:.cbkClosed").forEach(block=>scPaLib.findNode("des:a",block).onclick());
	},
	onLoad : function(){
		idKeyMgr.fetchJson(idKeyMgr.datasUrl, function (json) {
			try{
				idKeyMgr.loadDatas(json)
			}
			catch (e) {
				idKeyUi.alert(idKeyMgr.strings.error);
				console.error(e);
			}
		},function(error){
			idKeyUi.alert(idKeyMgr.strings.error);
		});
		const qs = io.parseQueryString(location.search);
		idKeyMgr.cid = qs.context === "select";
		if(qs.display === "tunneled") sc$("header").classList.add("hidden");

		scPaLib.findNodes("des:button.outBtn").forEach(btn=>btn.addEventListener("click", idKeyUi.changeTab));
		scPaLib.findNode("des:button.openNavMenu").addEventListener("click", function(event){
			sc$('navMenu').classList.toggle('showMenu');
			event.stopPropagation();
		});
		window.addEventListener("popstate",idKeyMgr.xReloadUrlState);
		scPaLib.findNodes("des:button.back2Criterions").forEach(function (backBtn){
			backBtn.addEventListener("click", function (event){
				const outBtn =scPaLib.findNode("des:button.outBtn", sc$("navMenu"));
				outBtn.click({"target":outBtn});
			});
		});
	},

	xBuildHTMLHistoCritLst : function(){
		idKeyMgr.reverseCritsState = {};
		if(idKeyMgr.state.length){
			const isClosed = sessionStorage.getItem("histoClosed") === "true";
			const bd = dom.newBd(scPaLib.findNode("des:div.keys",sc$("keys")));
			bd.elt("div","history "+ (isClosed ? "closed" : "opened"))
				.elt("button", "critHitoSize").att("title", isClosed ? idKeyMgr.strings.openHisto : idKeyMgr.strings.closeHisto).listen("click", idKeyMgr.onOpenCloseHisto)
					.elt("span").text(idKeyMgr.state.length).text(idKeyMgr.state.length > 1 ? idKeyMgr.strings.definedCrits : idKeyMgr.strings.definedCrit).up()
				.up()
				.elt("button", "reset").att("title", idKeyMgr.strings.reset).listen("click", idKeyMgr.onResetHisto)
					.elt("span").text(idKeyMgr.strings.reset).up()
				.up()
				.elt("div", "histo_content")
					idKeyMgr.state.forEach(function (step) {
						idKeyMgr.reverseCritsState[step.crit] = step.critVals;
						bd.current().appendChild(idKeyMgr.xBuildHTMLStep(step.crit,"history" ,step.critVals));
					});
				bd.up()
			.up();
		}
	},

	/**
	 * Ne sont publis que les critères :
	 *  - qui match tout les taxons restants (sauf si aucun dans ce cas qui match au moins un taxon)
	 *  - dont les values ne match pas chacune tous les taxons restants
	 *  - par ordre de plus petit maximum sélectionné par valeur
	 */
	xBuildHTMLNextCritLst : function(){
		// 1 critère qui match toutes la liste restante
			//-> par pouvoir dichotomique
		// 2 critère qui match un sous ensemble ->
			// par pouvoir dichotomique
		// 3 critères (masqués) qui ne match aucun élément de la liste
			// masqué -> par ordre alpha

		//indice dichotomique ->
			// inverse du Nbr de taxon max sélectionné
			//Plus max est faible, plus dicho score est élevé
		const dichotomyScore = function(values) {
			let max = 0;
			values.forEach(val=>max = val > max ? val : max);
			if(max === 0) return 0;
			return 1/max;
		}

		//Recup de la liste des critères restants.
		const crits = {};
		idKeyMgr.datas.secondaryCrits?.forEach(crit=>{
			if(!idKeyMgr.getHistoCrit(crit)) crits[crit] = true;
		});
		idKeyMgr.state.forEach(step=>step.critVals.forEach(critValName=>{
				const critVal = idKeyMgr.getCritVal(step.crit, critValName);
				critVal.secondaryCrits?.forEach(crit=>{
					if(!idKeyMgr.getHistoCrit(crit)) crits[crit] = true;
				});
		}));

		//Taxons de référence
		const curTaxMap = {};
		//On cherche les taxons qui matchent tous les critères.
		scPaLib.findNodes("des:li.taxon.selected", sc$("taxons")).forEach(node=>curTaxMap[node.getAttribute("data-taxon")] = true);
		//sinon, on cherche les taxons qui matchent une partie des critères.
		if(Object.keys(curTaxMap).length === 0) scPaLib.findNodes("des:li.taxon.partialSelected", sc$("taxons")).forEach(node=>curTaxMap[node.getAttribute("data-taxon")] = true);
		//Sinon, on charge tous les taxons.
		if(Object.keys(curTaxMap).length === 0) curTaxLst = idKeyMgr.datas.taxonsLst.forEach(taxon=>curTaxMap[taxon] = true);

		const matchAllTxnCrts = [], matchPartialTxnCrits = [];
		for(let critName in crits){
			if(idKeyMgr.reverseCritsState[critName]) continue;
			const selectableTxns = {};
			const txnsLengthByCritVal = {}
			for(let critValName in idKeyMgr.getCrit(critName).critvals){
				const reverseTaxons = idKeyMgr.getCritVal(critName, critValName).reverseTaxons;
				txnsLengthByCritVal[critValName] = 0;
				reverseTaxons.forEach(taxonsName=>{
					if(curTaxMap[taxonsName]){
						selectableTxns[taxonsName] = true;
						txnsLengthByCritVal[critValName]++;
					}
				});
			}
			const values = Object.values(txnsLengthByCritVal);
			const total = Object.keys(curTaxMap).length;
			let min = total;
			values.forEach(val=>{min = val < min ? val : min});
			const entry = {
				"crit":critName,
				"dichoScore":dichotomyScore(values),
				"minSel" : min,
				"selScore" : Object.keys(selectableTxns).length
			};

			if(entry.minSel === total) continue;
			else if(entry.selScore === total) matchAllTxnCrts.push(entry);
			else if(entry.selScore > 0 ) matchPartialTxnCrits.push(entry);
		}
		if(matchAllTxnCrts.length === 1) {
			matchPartialTxnCrits.push(matchAllTxnCrts.pop());
		}
		else if(matchAllTxnCrts.length === 0 && matchPartialTxnCrits.length === 1){
			const critName = matchPartialTxnCrits.pop().crit;
			idKeyMgr.loadCritDatas(critName, function(){
				const bd = dom.newBd(scPaLib.findNode("chi:div.keys",sc$("keys")));
				bd.current().append(idKeyMgr.xBuildHTMLCrit(critName, "popOnReturn"));
			});
			return;
		}
		//On tri d'abord par nombre de taxons sélectionné
		//Ensuite par dicho maximale
		const sort = function(a,b){
			if(a.selScore !== b.selScore) return a.selScore > b.selScore ? -1 : 1;
			return a.dichoScore > b.dichoScore ? -1 : 1;
		}
		const bd = dom.newBd(scPaLib.findNode("chi:div.keys",sc$("keys")));
		const critsToBuild = matchAllTxnCrts.length ? matchAllTxnCrts : matchPartialTxnCrits;
		if(critsToBuild.length === 0){
			bd.elt("div", "warning noMoreCrit")
				.elt("span").text(idKeyMgr.strings.noMoreCrits).up();
				idKeyMgr.xBuildHTMLCrit2TaxNavBtn(bd);
			bd.up();
			return;
		}
		critsToBuild.sort(sort);

		idKeyMgr.xBuildHTMLCritsList(critsToBuild, "mainCrits");
	},

	xBuildHTMLTaxonsLst : function(){
		idKeyMgr.datas.taxonsLst.forEach(taxon=>idKeyMgr.datas.taxons[taxon].score = 0);
		const histoCrits = scPaLib.findNodes("chi:div.keys/chi:div.history/des:div.history", sc$("keys"));
		histoCrits.forEach(histo=> {
			const matchedTaxons = {};
			const critName = idKeyMgr.getCritName(histo);
			const crit = idKeyMgr.getCrit(critName);
			if(crit.type === "mcq" || crit.type === "scq") histo.getAttribute("data-critVal").split(" ").forEach(critValName=>
				idKeyMgr.getCritVal(critName, critValName).reverseTaxons.forEach(taxon=> matchedTaxons[taxon] = true));
			else if(crit.type ==="slider"){
				const val = parseFloat(histo.getAttribute("data-critVal"));
				Object.keys(crit.critvals).forEach(k=>{
					const critval = crit.critvals[k];
					if(val>=critval.min && val <= critval.max) critval.reverseTaxons.forEach(taxon=> matchedTaxons[taxon] = true);
				});
			}
			for(let key in matchedTaxons) idKeyMgr.datas.taxons[key].score++;
		});

		idKeyMgr.datas.taxonsLst = Object.keys(idKeyMgr.datas.taxons).sort((a,b)=>{
			if(idKeyMgr.datas.taxons[a].score !== idKeyMgr.datas.taxons[b].score) return idKeyMgr.datas.taxons[a].score > idKeyMgr.datas.taxons[b].score ? -1:1;
			return idKeyMgr.datas.taxons[a].elisedCN < idKeyMgr.datas.taxons[b].elisedCN ? -1 : 1;
		});
		const bd = dom.newBd(sc$("taxlist"));
		bd.clear();
		bd.outTree();

		let taxoCount;

		bd.elt("div", "taxlistHead")
			.elt("span", "headTitle").text(idKeyMgr.strings.selection).up()
			.elt("span", "taxoCount"); taxoCount = bd.current(); bd.up();

			bd.elt("div", "filter")
				.elt("label")
					.elt("span").text(idKeyMgr.strings.filter).up()
					.elt("input").att("type", "text").att("role", "search").att("placeholder", idKeyMgr.strings.placeHolderTxns).att("data-filter","taxons").listen("keyup", idKeyMgr.onFilter).up()
					.elt("button").att("title", idKeyMgr.strings.applyFilter).listen("click", idKeyMgr.onFilter).elt("span").text(idKeyMgr.strings.applyFilter).up().up()
				.up()
			.up()
		.up();
		bd.elt("ul", histoCrits.length ? "sel":"");
		let count = histoCrits.length ? 0 : idKeyMgr.datas.taxonsLst.length;
		idKeyMgr.datas.taxonsLst.forEach(id=>{
			const taxon = idKeyMgr.datas.taxons[id];
			let matchClass;
			if(histoCrits.length){
				if(taxon.score === 0) matchClass = "unselected";
				else if(histoCrits.length === taxon.score) {
					matchClass = "selected";
					count++;
				}
				else matchClass = "partialSelected";
			}
			else matchClass = "noHistoCrits";
			bd.elt("li", "taxon " + id + " "+ matchClass).att("data-taxon",id).elt("a").att("href", "#").att("onclick", "idKeyUi.iframe('"+taxon.url+"',scPaLib.findNode('can:li',event.target)); return false;").elt("span").elt("span", "commonName").text(taxon.commonName).up();
			if(taxon.scientificName) bd.text(" (").elt("span", "scientificName").text(taxon.scientificName).up().text(")")
			if(histoCrits.length) bd.text(" ").elt("span", "score").att("title", taxon.score+(taxon.score > 1 ? idKeyMgr.strings.critsOn : idKeyMgr.strings.critOn) +histoCrits.length).text(taxon.score+"/"+histoCrits.length).up()
			bd.up().up().up();
		});
		bd.inTree()
		taxoCount.title = count + (count > 1 ? idKeyMgr.strings.selecteds : idKeyMgr.strings.selected);
		taxoCount.textContent = "("+count+")";
		const rootTaxoCount = sc$("taxoCount");
		rootTaxoCount.setAttribute("data-count", count)
		rootTaxoCount.textContent = "("+count+")";
		/*
		if(histoCrits.length){
			taxoCount.title = count + (count > 1 ? idKeyMgr.strings.selecteds : idKeyMgr.strings.selected);
			taxoCount.textContent = "("+count+")";
			const rootTaxoCount = sc$("taxoCount");
			rootTaxoCount.setAttribute("data-count", count)
			rootTaxoCount.textContent = "("+count+")";
		}
		else {
			sc$("taxoCount").textContent= "("+idKeyMgr.datas.taxonsLst.length+")";
			taxoCount.textContent = "("+count+")";

			const rootTaxoCount = sc$("taxoCount");
			rootTaxoCount.setAttribute("data-count", count)
			rootTaxoCount.textContent = "("+count+")";
		}*/
		idKeyMgr.xBuildHTMLTax2CritNavBtn(bd);
	},

	/**
	 * Constuit le contenu d'un critère dans un domBuilder
	 */
	xBuildHTMLCrit : function(critName, returnOpt, histoCritVals){
		const crit = idKeyMgr.getCrit(critName);
		if(!crit) console.error("Unable to build html criterion");

		const bd = dom.newBd(document.createDocumentFragment());
		if(!crit.loaded) {
			bd.elt("div", "loading").elt("span", "loading").text(idKeyMgr.strings.loading).up().up();
			idKeyMgr.addCritDatasLoadedCb(critName, function(){
				const node = idKeyMgr.xBuildHTMLCrit(critName,returnOpt, histoCritVals);
				const loading = scPaLib.findNode("des:div.loading");
				loading.insertAdjacentElement("afterend",node);
				loading.parentElement.removeChild(loading);
			});
			return bd.current();
		}

		let taxons;
		//Si premier critère (en ajout ou modifcation)
		if(!idKeyMgr.state.length || idKeyMgr.state.length==1 && idKeyMgr.state[0].crit === critName) taxons = idKeyMgr.datas.taxonsLst;
		else{
			const taxonsScore = {}
			let maxScore = 0;
			idKeyMgr.state.forEach(step=>{
				if(step.crit !== critName) {
					const matchedTaxons = {};
					step.critVals.forEach(critValName=>{
						idKeyMgr.getCritVal(step.crit, critValName).reverseTaxons.forEach(function (taxon) {
							matchedTaxons[taxon] = true;
						});
					});
					for(let key in matchedTaxons) taxonsScore[key] = taxonsScore[key]?taxonsScore[key]+1:1;
					maxScore++;
				}
			});
			taxons = [];
			for(let taxon in taxonsScore) if(taxonsScore[taxon] === maxScore) taxons.push(taxon);
		}

		const cancel = (event) => {
			if(returnOpt === "popOnReturn") {
				idKeyMgr.state.pop();
				idKeyMgr.nextSelStep(event);
			}
			else idKeyMgr.nextSelStep(event);
			idKeyMgr.xBrowserHisto("pushState");
		}

		bd.elt("div", "crit edit "+(histoCritVals?"histo":"")).prop("taxonsToMatch", taxons);
			bd.elt("div", "critHead");
				if(returnOpt !== "noReturn") bd.elt("button", "return").att("title", idKeyMgr.strings.return).listen("click", cancel).elt("span").text(idKeyMgr.strings.return).up().up();
				bd.elt("div", "illus");
					if (crit.illus)  bd.current().insertAdjacentHTML("afterbegin", crit.illus);
				bd.up()
				.elt("div", "content")
					.elt("span").text(crit.title).up()
				.up();
				if(crit.type === "mcq"){
					bd.elt("div","tools");
						bd.elt("button", "select").att("title", idKeyMgr.strings.valid).prop("disabled",histoCritVals && histoCritVals.length ? false : true).listen("click",idKeyMgr.onCritValMultiSelected).elt("span").text(idKeyMgr.strings.valid).up().up();
					bd.up();
				}
			bd.up();
			bd.elt("div", "critContent");
			if(crit.shortDesc) bd.current().insertAdjacentHTML("afterbegin",crit.shortDesc);
			if(crit.type === "scq" || crit.type === "mcq") {
				bd.elt("ul", "critVals").att("data-crit", critName);
				Object.keys(crit.critvals).forEach(function (critValName) {
					const critVal = crit.critvals[critValName];
					const histoSelected = histoCritVals && histoCritVals.indexOf(critValName) !== -1;
					bd.elt("li", "critVal " + (histoSelected ? "histoSelected selected" : "")).att("data-critVal", critValName).elt("div", "critBlk");
					if (crit.type === "mcq") bd.elt("a", "multival").att("title", idKeyMgr.strings.select).att("href", "#").att("onclick", "idKeyMgr.onClickCritValMulti(event); return false;")
					bd.elt("div", "illus")
					if (critVal.illus) bd.current().insertAdjacentHTML("afterbegin", critVal.illus);
					bd.up();
					bd.elt("div", "content")
						.elt("span", "value").text(critVal.value).up();
					if (critVal.shortDesc) bd.elt("span", "shortDesc").currentUp().insertAdjacentHTML("beforeend", critVal.shortDesc);
					bd.up();
					bd.elt("div", "tools");
					if (critVal.desc) bd.elt("button", "seeDetails").att("title", idKeyMgr.strings.seeDetails).listen("click", idKeyMgr.onOpenCritVal).elt("span").text(idKeyMgr.strings.details).up().up()
					if (crit.type === "scq" && !histoSelected) bd.elt("button", "select").att("title", idKeyMgr.strings.choose).listen("click", idKeyMgr.onCritValSelected).elt("span").text(idKeyMgr.strings.choose).up().up();
					bd.up();
					if (crit.type === "mcq") bd.up();
					bd.up().up();
				});
				bd.up();//ul
			}
			else if (crit.type === "slider"){
				let min = crit.min ? parseFloat(crit.min) : null;
				let max = crit.max ? parseFloat(crit.max) : null;
				const defaultVal = histoCritVals?.[0];
				Object.keys(crit.critvals).forEach(key=>{
					const critval = crit.critvals[key];
					if(min === null || min > critval.min) min = critval.min;
					if(max === null || max < critval.max) max = critval.max;
				});
				const step = crit.step ? parseFloat(crit.step) : (max-min)/100;
				const onInput = function(event){
					const input = scPaLib.findNode("can:input", event.target);

					const prev = scPaLib.findNode("psi:input", input);
					if(prev) prev.value = input.value;
					const next = scPaLib.findNode("nsi:input", input);
					if(next) next.value = input.value;

					scPaLib.findNode("nsi:button", input).disabled = false;
				}
				bd.elt("div", "critSlider").att("data-crit", critName);
				bd.elt("span", "minSlider").text(min).up();
				bd.elt("input", "slider").att("type", "range").att("min", min).att("max", max).att("value", defaultVal || (min+(max-min)/2)).att("step", step).listen("input", onInput).up();
				bd.elt("span", "maxSlider").text(max).up();

				bd.elt("input", "sliderTxt").att("type", "number").att("min", min).att("max", max).att("step", step).listen("input", onInput).att("placeHolder", defaultVal||(min+(max-min)/2));
				if(defaultVal) bd.current().value = defaultVal;
				bd.up();

				bd.elt("span", "unit").text(crit.unit).up();
				bd.elt("button", "select").att("title", idKeyMgr.strings.valid).prop("disabled",histoCritVals && histoCritVals.length ? false : true).listen("click",idKeyMgr.onCritContinuousSelected).elt("span").text(idKeyMgr.strings.valid).up().up();
				bd.up();
			}
		bd.up();//div.critContent
		idKeyMgr.resetMediaMgrs();

		return bd.current();
	},
	/**
	 * Construit un critère ou une étape d'historique
	 * ctx = "history", "exclusive", "regular"
	 */
	xBuildHTMLStep : function(critName, ctx, critVals){
		if(!idKeyMgr.getCrit(critName).loaded) idKeyMgr.loadCritDatas(critName);
		const crit = idKeyMgr.getCrit(critName);
		let firstCritVal = null;
		if(critVals && critVals.length === 1) firstCritVal = idKeyMgr.getCritVal(critName, critVals[0]);
		const bd = dom.newBd(document.createDocumentFragment());

		if(ctx !== "history")bd.elt("a", "selectCrit critBlck "+critName).att("href", "#").att("title", idKeyMgr.strings.select).att("onclick", "idKeyMgr.onCritSelected(event); return false;");
		bd.elt("div", "step "+(ctx?ctx:"") +(ctx === "history" ? " critBlck "+critName:"")).att("data-crit", critName);

		if(critVals) bd.att("data-critVal", critVals.join(" "));
			bd.elt("div", "illus")
				if(firstCritVal && firstCritVal.illus) bd.current().insertAdjacentHTML("afterbegin", firstCritVal.illus);
				else if (crit.illus)  bd.current().insertAdjacentHTML("afterbegin", crit.illus);
			bd.up()
			bd.elt("div", "content")
				.elt("span", "crit").text(crit.title).up();
				if(critVals) {
					if(crit.type === "mcq"){
						bd.elt("ul");
						for(let i = 0 ; i < critVals.length ; i++) bd.elt("li").elt("span", "critVal").text(idKeyMgr.getCritVal(critName, critVals[i]).value).up().up();
						bd.up();
					}
					else{
						bd.elt("span", "critVal").text(idKeyMgr.getCritVal(critName, critVals[0]).value).up()
					}
				}

			bd.up()
			if(critVals){
				bd.elt("div", "tools")
					.elt("button", "edit").att("title", idKeyMgr.strings.edit).listen("click",idKeyMgr.onEditHistory).elt("span").text(idKeyMgr.strings.edit).up().up()
					.elt("button", "remove").att("title", idKeyMgr.strings.remove).listen("click",idKeyMgr.onRemoveHistory).elt("span").text(idKeyMgr.strings.remove).up().up()
				bd.up();
			}
		bd.up();
		if(ctx !== "history") bd.up();
		return bd.current();
	},
	xBuildHTMLExclusiveCrits : function(exclusiveCrits, returnOpt){
		const bd = dom.newBd(scPaLib.findNode("chi:div.keys",sc$("keys")))/*.clear()*/;
		if(exclusiveCrits.length === 1) bd.current().appendChild(idKeyMgr.xBuildHTMLCrit(exclusiveCrits[0], returnOpt ? returnOpt : "popOnReturn"));
		else idKeyMgr.xBuildHTMLCritsList(exclusiveCrits, "exclusiveCrits");
	},

	xBuildHTMLCritsList : function(list, code){
		const bd = dom.newBd(scPaLib.findNode("chi:div.keys",sc$("keys")));

		bd.elt("div", "critlist");

		bd.elt("div","critListHead")
			.elt("span", "headTitle").text(idKeyMgr.strings.selectNewCriterion).up()

			.elt("div", "filter")
				.elt("label")
					.elt("span").text(idKeyMgr.strings.filter).up()
					.elt("input").att("type", "text").att("placeholder", idKeyMgr.strings.placeHolderKeys).att("data-filter","keys").listen("keyup", idKeyMgr.onFilter).up()
					.elt("button").att("title", idKeyMgr.strings.applyFilter).listen("click", idKeyMgr.onFilter).elt("span").text(idKeyMgr.strings.applyFilter).up().up()
				.up()
			.up()
		.up();

		bd.elt("div", code)
		/*autorise la liste d'objet json crit ou des critname uniquement */
		list.forEach(crit=> bd.current().appendChild(idKeyMgr.xBuildHTMLStep(crit.crit ? crit.crit : crit)));
		bd.up();
		idKeyMgr.xBuildHTMLCrit2TaxNavBtn(bd);
		bd.up();
	},

	xBuildHTMLCrit2TaxNavBtn : function(bd){
		const count = parseInt(sc$("taxoCount").getAttribute("data-count"));
		const taxLst = scPaLib.findNodes("des:li", sc$("taxlist"));
		//pas de bouton si pas de sélection
		if (count === taxLst.length) return;
		if(count === 1){
			const a = scPaLib.findNode("chi:a", taxLst[0]);
			bd.elt("button", "mobileFixedNav taxFound").att("title", a.textContent).listen("click", function(){a.click({"target": a})}).elt("span").text(a.textContent).up().up();
		}
		else{
			bd.elt("button", "mobileFixedNav seeTaxLst").att("title", count +idKeyMgr.strings.selecteds).listen("click", function(){
				const taxoCount = sc$("taxoCount");
				taxoCount.click({"target": taxoCount})
			}).elt("span").text(count +idKeyMgr.strings.selecteds).up().up();
		}
	},

	xBuildHTMLTax2CritNavBtn : function(bd){
		bd.elt("button", "mobileFixedNav seeCrits").att("title", idKeyMgr.strings.returnToCrits).listen("click", function(){
			const btn = scPaLib.findNode("des:button.keys", sc$("navMenu"));
			btn.click({"target":btn});
		}).elt("span").text(idKeyMgr.strings.returnToCrits).up().up();
	},
	/**
	 * Vérification qu'un critère est disponible selon l'état actuel
	 * @param crit
	 */
	xCleanHisto : function(index){
		let cleaned = 0;
		const isAvailableCrit = function(crit, toIndex){
			if(idKeyMgr.datas.exclusiveCrits?.indexOf(crit) !== -1) return true;
			else if(idKeyMgr.datas.secondaryCrits?.indexOf(crit) !== -1) return true;
			else for(let i = 0 ; i < toIndex ; i++) {
					const step = idKeyMgr.state[i];
					for(let j = 0 ; j < step.critVals.length ; j++){
						const critVal = idKeyMgr.getCritVal(step.crit, step.critVals[j]);
						if(critVal.exclusiveCrits?.indexOf(crit) !== -1) return true;
						else if(critVal.secondaryCrits?.indexOf(crit) !== -1) return true;
					}
				}
			return false;
		}
		//Netoyage des des critères qui n'ont pas de raison d'être
		while (index < idKeyMgr.state.length){
			if(!isAvailableCrit(idKeyMgr.state[index].crit, index))  {
				idKeyMgr.state.splice(index,1);
				cleaned++;
			}
			else index++;
		}
		return cleaned;
	},
	/**
	 * Ajoute ou remplace une entrée d'historique
	 * @param fct : pushState/replaceState
	 */
	xBrowserHisto : function(fct){
		if(idKeyMgr.inReloadUrlState && fct === "pushState") return ;
		const tab = scPaLib.findNode("chi:div.tab.selected", sc$("main")).id;
		let taxon = scPaLib.findNode("bod:/chi:dialog/des:iframe");
		const editCrit = scPaLib.findNode("des:div.crit.edit", sc$("keys"));
		if(taxon) taxon = taxon.getAttribute("data-taxon");
		let str = location.origin + location.pathname +"?state="+JSON.stringify(idKeyMgr.state)+"&tab="+tab + (taxon?"&taxon="+taxon:"");
		const qs = io.parseQueryString(location.search)
		if(qs.mode) str += "&mode="+qs.mode;
		if(qs.context) str += "&context="+qs.context;
		if(qs.display) str += "&display="+qs.display;
		if(editCrit)
			if(editCrit.classList.contains("histo")) str += "&editHisto=" + (scPaLib.findNode("des:ul", editCrit)||scPaLib.findNode("des:div.critSlider", editCrit)).getAttribute("data-crit");
			else str += "&editCrit=" + (scPaLib.findNode("des:ul", editCrit) || scPaLib.findNode("des:div.critSlider", editCrit)).getAttribute("data-crit");
		if(str === window.location.toString()) return;
		history[fct](idKeyMgr.state, "",str);
	},

	xValidCritValSelection : function(critName, val){
		const editedIndex = idKeyMgr.getHistoCritIndex(critName);
		const insertToIndexFct = function(){
			for(let i = 0 ; i < idKeyMgr.state.length ; i++){
				if(i===0) {if (idKeyMgr.datas.exclusiveCrits?.indexOf(idKeyMgr.state[0].crit) === -1) {return 0;}}
				else {
					for(let j = 0 ; j < idKeyMgr.state[i - 1].critVals.length ; j++){
						const critVal = idKeyMgr.getCritVal(idKeyMgr.state[i - 1].crit, idKeyMgr.state[i - 1].critVals[j]);
						if (critVal.exclusiveCrits?.indexOf(idKeyMgr.state[i].crit) === -1) return i;
					}
				}
			}
			return -1;
		}
		const insertToIndex = insertToIndexFct(critName);

		if(editedIndex !== -1) {
			idKeyMgr.state[editedIndex].critVals = val;
			idKeyMgr.xCleanHisto(editedIndex);
		}
		else if(insertToIndex !== -1) idKeyMgr.state.splice(insertToIndex,0,{"crit":critName,"critVals":val});
		else idKeyMgr.state.push({"crit":critName,"critVals":val});

		idKeyUi.xClose();
		idKeyMgr.nextSelStep();
		idKeyMgr.xBrowserHisto("pushState");
	},

	xReloadUrlState : function () {
		idKeyMgr.inReloadUrlState = true;
		const qs = io.parseQueryString(location.search);
		if(qs) try{
			const state = qs.state ? JSON.parse(qs.state) : [];
			const critsToLoad = [];
			let reloaded = false;
			const cb = function(){
				if(reloaded) return;
				for(let i = 0 ; i < critsToLoad.length ; i++) if(!idKeyMgr.getCrit(critsToLoad[i]).loaded) return;
				reloaded = true;

				if(state.length) {
					for(let i = 0 ; i < state.length ; i ++) {
						if(!idKeyMgr.getCrit(state[i].crit))  {
							state.splice(i--,1);
							continue;
						}
						for(let j = 0 ; j < state[i].critVals ; j++) if(!idKeyMgr.getCritVal(state[i].crit, state[i].critVals[j])) state[i].critVals.splice(j--,1);
						if(! state[i].critVals.length) state.splice(i--,1);
						if(idKeyMgr.xCleanHisto(i)) i--;
					}
				}
				idKeyMgr.state = state;
				idKeyUi.xClose();
				idKeyMgr.nextSelStep();
				let btn = scPaLib.findNode("des:button.outBtn."+qs.tab, sc$("navMenu")) || scPaLib.findNode("des:button.outBtn."+qs.tab, sc$("footer"));
				if(btn) btn.click({"target":btn});
				if(qs.editHisto){
					btn = scPaLib.findNode("des:.critBlck."+qs.editHisto+"/des:button.edit", scPaLib.findNode("des:div.histo_content", sc$("keys")));
					if(btn) btn.click({"target":btn});
				}
				if(qs.editCrit){
					btn = scPaLib.findNode("des:a.critBlck."+qs.editCrit+"/chi:div.step", scPaLib.findNode("des:div.critlist", sc$("keys")));
					if(btn) btn.click({"target":btn});
				}
				if(qs.taxon){
					const lnk = scPaLib.findNode("des:li."+qs.taxon+"/des:a", sc$("main"));
					if(lnk) lnk.click({"target":lnk});
				}
				idKeyMgr.xBrowserHisto("replaceState");
				idKeyMgr.inReloadUrlState = false;
			}
			if(state.length) {
				for(let i = 0 ; i < state.length ; i ++) {
					if (!idKeyMgr.getCrit(state[i].crit)) state.splice(i--, 1);
					else critsToLoad.push(state[i].crit);
				}
				for(let i = 0 ; i < critsToLoad.length ; i++) idKeyMgr.loadCritDatas(critsToLoad[i], cb);
			}
			else cb();
		}
		catch(e){
			console.error("error in state recovery",e);
		}
	},
	fetchJson : function(url,cb, cberror){
		if("fetch" in window){
			fetch(url).then(response=> {
				if(!response.ok){if(cberror) cberror.call(null, response);}
				return response.json();
			}).then(json =>cb.call(null,json));
		}
		else {
			const fetchDatas = io.openHttpRequest(url);
			fetchDatas.onload = function(event){
				if(event.status >= 300) { if(cberror) cberror.call(null, event.target);}
				else cb.call(null,JSON.parse(event.target.responseText));
			}
			fetchDatas.send();
		}
	}
}

const idKeyUi = {
	alert : function(txt,cb,title, insert){
		idKeyUi.xClose();
		const bd = dom.newBd(document.body);
		bd.elt("dialog", "alert").listen("close", idKeyUi.xClose)
			.elt("div", "close").elt("button", "close").att("title", idKeyMgr.strings.close).listen("click", cb?cb:idKeyUi.xClose).elt("span").text(idKeyMgr.strings.close).up().up().up()
			.elt("div", "modal_content");
				if(title) bd.elt("div", "modalTitle").elt("h2").text(title).up().up();
				if(insert) {
					bd.current().insertAdjacentHTML("beforeend", txt);
					idKeyMgr.resetMediaMgrs();
				}
				else{
					bd.elt("span").text(txt).up()
				}
			bd.up()
			.elt("div","tools")
				.elt("button", "valid").att("title", idKeyMgr.strings.close).listen("click",("click",function(event){idKeyUi.xClose();cb?cb(event):null;})).elt("span").text(idKeyMgr.strings.close).up().up()
			.up().current().showModal();
	},
	confirm : function(txt,cb, type){
		idKeyUi.xClose();
		const bd = dom.newBd(document.body);
		bd.elt("dialog", "confirm")
			.elt("div", "close").elt("button", "close").att("title", idKeyMgr.strings.close).listen("click", idKeyUi.xClose).elt("span").text(idKeyMgr.strings.close).up().up().up()
			.elt("div", "modal_content").elt("span");
				if(type && type === "html") bd.current().insertAdjacentHTML("afterbegin",txt);
				else bd.text(txt)
				bd.up().up()
			.elt("div","tools")
				.elt("button", "cancel").att("title", idKeyMgr.strings.cancel).listen("click",idKeyUi.xClose).elt("span").text(idKeyMgr.strings.cancel).up().up()
				.elt("button", "valid").att("title", idKeyMgr.strings.valid).listen("click",function(event){idKeyUi.xClose();cb(event);}).elt("span").text(idKeyMgr.strings.valid).up().up()
			.up().current().showModal();
	},
	/*modal : function(node,cb){
		idKeyUi.xClose();
		const bd = dom.newBd(document.body);
		bd.elt("dialog", "modal")
			.elt("div", "close").elt("button", "close").att("title", idKeyMgr.strings.close).listen("click", idKeyUi.xClose).elt("span").text(idKeyMgr.strings.close).up().up().up()
			.elt("div", "modal_content")
				.current().appendChild(node);
			bd.up()
			bd.elt("div","tools")
				.elt("button", "cancel").att("title", idKeyMgr.strings.cancel).listen("click",idKeyUi.xClose).elt("span").text(idKeyMgr.strings.cancel).up().up()
				.elt("button", "valid").att("title", idKeyMgr.strings.valid).listen("click",("click", function(event){idKeyUi.xClose();cb(event);})).elt("span").text(idKeyMgr.strings.valid).up().up()
			.up()
		.up().current().showModal();
		idKeyMgr.resetMediaMgrs();
	},*/
	iframe : function(url, navCtx){
		if(url.startsWith("co/")) url = url.substring(3);
		idKeyUi.xClose();
		const bd = dom.newBd(document.body);
		bd.elt("dialog", "modal")
			.elt("div", "close").elt("button", "close").att("title", idKeyMgr.strings.close).listen("click", idKeyUi.xClose).elt("span").text(idKeyMgr.strings.close).up().up().up()
			.elt("div", "modal_content");
		if(navCtx){
			const prev = scPaLib.findNode("psi:li.taxon",navCtx);
			const next = scPaLib.findNode("nsi:li.taxon",navCtx);
			if(prev || next || idKeyMgr.cid){
					bd.elt("div", "navtools");
						if(prev) bd.elt("button", "previous").att("title", idKeyMgr.strings.previous).listen("click", function(event){
							idKeyUi.iframe(prev.getAttribute("data-taxon-url") || idKeyMgr.datas.taxons[prev.getAttribute("data-taxon")].url,prev);
						}).elt("span").text(scPaLib.findNode("chi:a",prev).textContent).up().up();
						else bd.elt("button", "previous").att("title", idKeyMgr.strings.previous).att("disabled", "true")
							.elt("span").text(idKeyMgr.strings.previous).up().up();

						if(idKeyMgr.cid) bd.elt("button", "selectTaxo").att("title", idKeyMgr.strings.selectTitle).listen("click", function(event){
							const taxon = idKeyMgr.datas.taxons[navCtx.getAttribute("data-taxon")]
							const datas = {};
							for(let key in taxon){
								if(key === "url" || key === "critvals" || key === "score" || key === "elisedCN") continue;
								datas[key] = taxon[key];
							}
							datas['cidInteraction'] = "ended";
							if(window.parent) window.parent.postMessage(datas, '*');
							else console.log(datas);
						}).elt("span").text(idKeyMgr.strings.select).up().up();

						if(next) bd.elt("button", "next").att("title", idKeyMgr.strings.next).listen("click", function(event){
							idKeyUi.iframe(next.getAttribute("data-taxon-url") || idKeyMgr.datas.taxons[next.getAttribute("data-taxon")].url,next);
						}).elt("span").text(scPaLib.findNode("chi:a",next).textContent).up().up();
						else bd.elt("button", "next").att("title", idKeyMgr.strings.next).att("disabled", "true")
							.elt("span").text(idKeyMgr.strings.next).up().up();
					bd.up();
				}
			}
			bd.elt("iframe").att("src", url).att("data-taxon", navCtx.getAttribute("data-taxon")).att("title", scPaLib.findNode("des:a/des:span", navCtx).innerText).up()
		.up().current().showModal()
		idKeyMgr.xBrowserHisto("pushState");
	},
	changeTab : function(event){
		const button = scPaLib.findNode("can:button.outBtn", event.target);
		scPaLib.findNodes("des:button.outBtn",sc$("navMenu")).forEach(node=>{
			if(node !== button) node.classList.remove("selected");
			else node.classList.add("selected");
		});
		scPaLib.findNodes("des:button.outBtn",sc$("footer")).forEach(node =>{
			if(node !== button) node.classList.remove("selected");
			else node.classList.add("selected");
		});
		scPaLib.findNodes("chi:div.tab", sc$("main")).forEach(tab=>{
			if(tab.id === button.getAttribute("data-tab")) {
				tab.classList.add("selected");
			}
			else {
				tab.classList.remove("selected");
			}
		});
		idKeyMgr.xBrowserHisto("pushState");
	},
	xClose : function(){
		const mod = scPaLib.findNode("des:dialog");
		if(mod) mod.parentElement.removeChild(mod);
		if(scPaLib.findNode("des:iframe", mod)) idKeyMgr.xBrowserHisto("pushState");
	},
	xCloseMenus : function (){
		sc$("navMenu").classList.remove("showMenu");
		const extMenu = sc$('extendedMenu');
		if(extMenu) extMenu.classList.remove('showMenu');
	},
	xHistoHtmlChange(){
		const keysDiv = scPaLib.findNode("chi:div.keys", sc$("keys"));
		if(keysDiv.children.length === 2 && keysDiv.children[0].offsetHeight > keysDiv.offsetHeight/2) keysDiv.classList.add("scroller");
		else keysDiv.classList.remove("scroller");
	}
};
/**
 * Gestionnaire de l'app
 */
const idKeyApp = scOnLoads[scOnLoads.length] = {
	event : null,
	mode : null,
	inApp : null,
	worker:null,
	newWorker:null,

	/**
	 * page loaded
	 */
	onLoad : function(){
		idKeyApp.inApp = navigator.standalone || window.matchMedia('(display-mode: standalone)').matches || io.parseQueryString(location.search).mode === "app";
		navigator.serviceWorker.ready.then(registration=>{
			idKeyApp.worker = registration.active;
			if(registration.waiting){
				idKeyApp.newWorker = registration.waiting;
				setTimeout(()=>  idKeyApp.onUpdateAvailable(null, true), 2000);
			}
			if(sessionStorage.getItem("downloadOnStart") === "true") {
				idKeyApp.downloadCaches(true);
				sessionStorage.setItem("downloadOnStart", null);
			}
			else {
				if(idKeyApp.inApp) idKeyApp.postMessage({"type":"isFullyOffline"});
			}
			navigator.onLine ? idKeyApp.onOnline() : idKeyApp.onOffline();
		});
	},
	postMessage(message, worker){
		const channel = new MessageChannel();
		channel.port1.onmessage = idKeyApp.onSwMessage;
		(worker || idKeyApp.worker).postMessage(message, [channel.port2]);
	},

	onOnline : function(){sc$("mobileZone").hidden = false;},
	onOffline : function(){if(!idKeyApp.mode === "offline") sc$("mobileZone").hidden = true;},

	/**
	 * interception du prompt d'install
	 */
	onBeforeInstallPrompt : function(event){
		// Prevent Chrome <= 67 from automatically showing the prompt
		event.preventDefault();
		idKeyApp.event = event;
		dom.newBd(sc$("mobileZone")).elt("button", "install mobile").att("title", idKeyMgr.strings.installApp).listen("click", idKeyApp.onInstallApp).elt("span").text(idKeyMgr.strings.installApp).up().up();
	},
	/**
	 * Lancement du prompt d'install
	 */
	onInstallApp : function(event){
		const btn = scPaLib.findNode("can:button", event.target);
		btn.parentElement.removeChild(btn);
		idKeyApp.event.prompt();
	},

	/**
	 * Message de warning update & download ou offline
	 */
	onPrepareOffline : function(event, message, size){
		message = message || (idKeyApp.mode === "offline" ? idKeyMgr.strings.updateWarning : idKeyMgr.strings.offlineWarning);
		const rawSize = size ? size : scPaLib.findNode("can:button",event.target).appSize;
		size = idKeyApp.xGetHumanReadableSize(rawSize);

		const onError = function(){
			console.log("Unable to user navigator.storage. Try to bypass");
			idKeyApp.downloadCaches();
		}
		const onSpaceSuccess = function(){
			idKeyUi.confirm(message.replace("%s", size), function(){
				if (navigator.storage && navigator.storage.persist){
					navigator.storage.persist().then(persistent=> {
						if (persistent) {
							if(idKeyApp.mode === "offline") {
								idKeyApp.xDownloading();
								sessionStorage.setItem("downloadOnStart", "true");
								idKeyApp.postMessage({"type":"clearCache"});
							}
							else idKeyApp.downloadCaches();
						}
						else idKeyUi.alert(idKeyMgr.strings.featureNotAvailable);
					});
				}
				else onError();
			}, "html");
		};

		//handle space
		new Promise(function (resolve, reject){
			if (navigator.storage && navigator.storage.estimate) {
				navigator.storage.estimate().then(function (storage) {
					if((storage.quota + storage.usage) > rawSize) resolve();
					else idKeyUi.alert(idKeyMgr.strings.notEnoughSpace.replace("%s", size).replace("%s", idKeyApp.xGetHumanReadableSize(storage.quota + storage.usage)));
				}).catch(reject);
			}
			else resolve();
		}).then(onSpaceSuccess)
			.catch(onError);
	},
	/**
	 * gestion de message du SW
	 */
	onSwMessage : function(event){
		if(event.data){
			switch (event.data.type) {
				case "offlineReady":
					idKeyApp.xDownloaded();
					break;
				case "offlineNotReady":
					dom.newBd(sc$("mobileZone")).clear()
						.elt("button", "offline mobile").prop("appSize", event.data.appSize).att("title", idKeyMgr.strings.prepareOffline).listen("click", idKeyApp.onPrepareOffline)
							.elt("span").text(idKeyMgr.strings.prepareOffline).up()
						.up();
					idKeyApp.mode = "online";
					break;
				case "allCacheInstalled":
					idKeyUi.alert(idKeyMgr.strings.offlineReady)
					idKeyApp.xDownloaded();

					break;
				case "cacheCleared":
					idKeyApp.postMessage({"type":"activate"}, idKeyApp.newWorker);
					break;
				case "installationFailed":
					idKeyUi.alert(idKeyMgr.strings.offlineFailed, function(){window.location.reload();});
					break;
				case "updateSize":
					dom.newBd(sc$("mobileZone")).clear().elt("button", "update mobile").prop("appSize", event.data.appSize).att("title", idKeyMgr.strings.prepareUpdate).listen("click", idKeyApp.onPrepareOffline)
						.elt("span").text(idKeyMgr.strings.prepareUpdate).up()
					.up();
					if(!event.data.onStart) idKeyApp.onPrepareOffline(null, idKeyMgr.strings.updateOfflineReady+idKeyMgr.strings.updateWarning, event.data.appSize);
			}
		}
	},

	/**
	 * Lancement d'un update d'un nouveau SW
	 */
	onUpdateAvailable : function(newWorker, onStart){
		if(newWorker) idKeyApp.newWorker = newWorker;
		navigator.serviceWorker.addEventListener('controllerchange', function () {if(idKeyApp.newWorker) window.location.reload();});
		if(idKeyApp.mode === "online" || !idKeyApp.inApp) {
			if(onStart) idKeyApp.postMessage({"type":"clearCache"});
			else idKeyUi.alert(idKeyMgr.strings.updateReady,function(){idKeyApp.postMessage({"type":"clearCache"});});
		}
		else if(idKeyApp.mode === "offline") idKeyApp.postMessage({"type":"fetchNewSize", "onStart":onStart}, idKeyApp.newWorker);
		else console.log("DEBUG: Erreur dans le process de maj");
	},

	/**
	 * Lancement du download
	 */
	downloadCaches : function (skipAlert){
		idKeyApp.postMessage({"type":"loadOffline"});
		if(!skipAlert) idKeyUi.alert(idKeyMgr.strings.offlineLoadingStarted);
		idKeyApp.xDownloading();
		idKeyApp.xPingWS();
	},
	xDownloading : function(){dom.newBd(sc$("mobileZone")).clear().elt("span", "downloading mobile").att("title", idKeyMgr.strings.downloading).elt("span").text(idKeyMgr.strings.downloading).up().up();},
	xDownloaded : function(){
		idKeyApp.mode = "offline";
		dom.newBd(sc$("mobileZone")).clear().elt("span", "downloaded mobile").att("title", idKeyMgr.strings.downloaded).elt("span").text(idKeyMgr.strings.downloaded).up().up();
	},
	/** en cas d'installation longue, le SW peut stopper si pas d'interaction avec un client **/
	xPingWS : function(){
		if(scPaLib.findNode("chi:span.mobile", sc$("mobileZone"))) setTimeout(function (){
			idKeyApp.postMessage({"type":"ping"});
			idKeyApp.xPingWS();
		},1000);
	},
	xGetHumanReadableSize : function(size){
		let counter = 0;
		while (size > 1000){
			size /= 1024;
			counter++
		}
		size = Math.round(size);
		switch (counter) {
			case 0:size +="octets";break;
			case 1:size +="Ko";break;
			case 2:size +="Mo";break;
			case 3:size +="Go";break;
			case 4:size +="To";break;
		}
		return size;
	}
}
window.addEventListener('beforeinstallprompt', idKeyApp.onBeforeInstallPrompt);
window.addEventListener('appinstalled', function(event){setTimeout(function (){idKeyApp.postMessage({"type":"isFullyOffline"});},200);});
window.addEventListener('online', idKeyApp.onOnline);
window.addEventListener('offline', idKeyApp.onOffline);
window.addEventListener('message', idKeyApp.onSwMessage);
window.addEventListener('click', idKeyUi.xCloseMenus);
