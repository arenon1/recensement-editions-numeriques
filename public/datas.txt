{"taxons":{"BD5TVkTfvzkoRsNtCpgoDb":{"commonName":"Trésor de la Langue Française informatisé","otherName":"TLFi","Lien vers le projet":"http:\/\/atilf.atilf.fr\/","Lien vers IIIF":"IIIF","url":"TLFi.html","critvals":["sBAvjiNl2hjXswp3uyWJdh","dSStNXgPCQgXAIHAyWgmk","ivdk2sTtvb5z4BLh4CdIb","Iv1bOVkTugczUE51LaRX8c","fE9UOV46m2bC39iW4tq1kh","CRx7TwmYBif8XJK8jbuwY","xKaNp7QqjulLLOjXYcOCki","gIMJ3bhOVhbXprlGGYOged","KGDX8Nh5FgcurthuGkovMi","xjyw7kKZLWffNPu4GhT8jb","Vg34oJ1Hn7bkpUJcl9GHib","AhgklmDzVijXwkKarvVRwf","mWcJ90CbdUfiKKOeqGRU4b","ccmRJsIEyUbLfwK8X3Avce","eC5PP5UOKKcZVCXHg4K60e"]},"kOhUBzjzWefRxohrVlcV6e":{"commonName":"Nénufar","Corpus du projet":"Dictionnaire","Lien vers l'ouvrage numérisé":"Le petit Larousse illustré de 1922","Lien vers le projet":"http:\/\/nenufar.huma-num.fr\/","Porteur du projet":"Hervé Bohbot","Pays d'origine":"France","Date début\/fin projet":"Depuis le 01\/01\/01 (toujours en cours)","Institution":"Université de Montpellier","url":"nenufar.html","critvals":["V5S3nu3eRHfVrywTVNVefj","sBAvjiNl2hjXswp3uyWJdh","Iv1bOVkTugczUE51LaRX8c","CRx7TwmYBif8XJK8jbuwY","KGDX8Nh5FgcurthuGkovMi","xjyw7kKZLWffNPu4GhT8jb","mWcJ90CbdUfiKKOeqGRU4b","AhgklmDzVijXwkKarvVRwf","ccmRJsIEyUbLfwK8X3Avce","eC5PP5UOKKcZVCXHg4K60e"]},"BD5TVkTfvzkoRsNtCpgoDb":{"commonName":"Trésor de la Langue Française informatisé","otherName":"TLFi","Lien vers le projet":"http:\/\/atilf.atilf.fr\/","Lien vers IIIF":"IIIF","url":"TLFi.html","critvals":["sBAvjiNl2hjXswp3uyWJdh","dSStNXgPCQgXAIHAyWgmk","ivdk2sTtvb5z4BLh4CdIb","Iv1bOVkTugczUE51LaRX8c","fE9UOV46m2bC39iW4tq1kh","CRx7TwmYBif8XJK8jbuwY","xKaNp7QqjulLLOjXYcOCki","gIMJ3bhOVhbXprlGGYOged","KGDX8Nh5FgcurthuGkovMi","xjyw7kKZLWffNPu4GhT8jb","Vg34oJ1Hn7bkpUJcl9GHib","AhgklmDzVijXwkKarvVRwf","mWcJ90CbdUfiKKOeqGRU4b","ccmRJsIEyUbLfwK8X3Avce","eC5PP5UOKKcZVCXHg4K60e"]}},"exclusiveCrits":["A0Z6kFFqWxjgjYJA1XukGf","CCjOwUv47VgbUm2XNVXiwg","lM7q60egLdjiTjBRR4AyA","xjzzJo9sTZe6u2w6q9wl6b","rlP56uZDxhjgJtD2HVtVmc","tALPyCtlungye1XILCSdid"],"crits":{"A0Z6kFFqWxjgjYJA1XukGf":{"type":"mcq","title":"Données d'entrée (acquisition)","critvals":{"eC5PP5UOKKcZVCXHg4K60e":{"reverseTaxons":["BD5TVkTfvzkoRsNtCpgoDb",
"kOhUBzjzWefRxohrVlcV6e"
]},"NXCVZkVfFobYs3wnpuSRKb":{"reverseTaxons":[
]},"HaIAyVRn9hfoIz035VEXVd":{"reverseTaxons":[
]}},"descUrl":"A0Z6kFFqWxjgjYJA1XukGf.txt"},"xjzzJo9sTZe6u2w6q9wl6b":{"type":"mcq","title":"Modalités de consultation et d'accès aux données produites","critvals":{"KGDX8Nh5FgcurthuGkovMi":{"reverseTaxons":["BD5TVkTfvzkoRsNtCpgoDb",
"kOhUBzjzWefRxohrVlcV6e"
]},"SzSyzvlVJFhYtzXpcYV0fc":{"reverseTaxons":[
]},"xjyw7kKZLWffNPu4GhT8jb":{"reverseTaxons":["BD5TVkTfvzkoRsNtCpgoDb",
"kOhUBzjzWefRxohrVlcV6e"
]},"KoUFKXnSsgeuSrmGalcmnb":{"reverseTaxons":[
]}},"descUrl":"xjzzJo9sTZe6u2w6q9wl6b.txt"},"rlP56uZDxhjgJtD2HVtVmc":{"type":"mcq","title":"Fonctionnalités proposées","critvals":{"Vg34oJ1Hn7bkpUJcl9GHib":{"reverseTaxons":["BD5TVkTfvzkoRsNtCpgoDb"
]},"AhgklmDzVijXwkKarvVRwf":{"reverseTaxons":["BD5TVkTfvzkoRsNtCpgoDb",
"kOhUBzjzWefRxohrVlcV6e"
]},"mWcJ90CbdUfiKKOeqGRU4b":{"reverseTaxons":["BD5TVkTfvzkoRsNtCpgoDb",
"kOhUBzjzWefRxohrVlcV6e"
]},"rffqS5egygbd5s7STDD6Ch":{"reverseTaxons":[
]},"QhDELlOlW8gDONF9kDm6ph":{"reverseTaxons":[
]}},"descUrl":"rlP56uZDxhjgJtD2HVtVmc.txt"},"tALPyCtlungye1XILCSdid":{"type":"mcq","title":"Langues","critvals":{"ccmRJsIEyUbLfwK8X3Avce":{"reverseTaxons":["BD5TVkTfvzkoRsNtCpgoDb",
"kOhUBzjzWefRxohrVlcV6e"
]},"SlPfNmJb98hvw8fW9B3cWc":{"reverseTaxons":[
]},"UQS5IXCy9FhOmAMiZyjNwj":{"reverseTaxons":[
]},"dauW6gbCd1esA8d7CNaj3i":{"reverseTaxons":[
]},"EkxgbwYNMKfUAtEgAidhGf":{"reverseTaxons":[
]},"MkQYIhP686cyD9erbdarNg":{"reverseTaxons":[
]},"Nin47VVMFQb3qR7iMY8N4":{"reverseTaxons":[
]},"QScs5CstmegKq1JZ5pXeXc":{"reverseTaxons":[
]},"TPKhxqtljlinGXm6rLbqEf":{"reverseTaxons":[
]}},"descUrl":"tALPyCtlungye1XILCSdid.txt"},"lM7q60egLdjiTjBRR4AyA":{"type":"mcq","title":"Opérations réalisées et outils numériques associés","critvals":{"Iv1bOVkTugczUE51LaRX8c":{"reverseTaxons":["BD5TVkTfvzkoRsNtCpgoDb",
"kOhUBzjzWefRxohrVlcV6e"
]},"fE9UOV46m2bC39iW4tq1kh":{"reverseTaxons":["BD5TVkTfvzkoRsNtCpgoDb"
]},"SbxUApQQOFjSr4e7u5lBVf":{"reverseTaxons":[
]},"CRx7TwmYBif8XJK8jbuwY":{"reverseTaxons":["BD5TVkTfvzkoRsNtCpgoDb",
"kOhUBzjzWefRxohrVlcV6e"
]},"xKaNp7QqjulLLOjXYcOCki":{"reverseTaxons":["BD5TVkTfvzkoRsNtCpgoDb"
]},"gIMJ3bhOVhbXprlGGYOged":{"reverseTaxons":["BD5TVkTfvzkoRsNtCpgoDb"
]},"w6v1VdLmhFb3kWuVEyOwld":{"reverseTaxons":[
]},"VMED7Vn7pnfvmPB3lTQJAf":{"reverseTaxons":[
]}},"descUrl":"lM7q60egLdjiTjBRR4AyA.txt"},"CCjOwUv47VgbUm2XNVXiwg":{"type":"mcq","title":"Objectifs et productions","critvals":{"V5S3nu3eRHfVrywTVNVefj":{"reverseTaxons":["kOhUBzjzWefRxohrVlcV6e"
]},"sBAvjiNl2hjXswp3uyWJdh":{"reverseTaxons":["BD5TVkTfvzkoRsNtCpgoDb",
"kOhUBzjzWefRxohrVlcV6e"
]},"dSStNXgPCQgXAIHAyWgmk":{"reverseTaxons":["BD5TVkTfvzkoRsNtCpgoDb"
]},"ivdk2sTtvb5z4BLh4CdIb":{"reverseTaxons":["BD5TVkTfvzkoRsNtCpgoDb"
]},"YknbTb6Sr2f6oHStK2MMJc":{"reverseTaxons":[
]}},"descUrl":"CCjOwUv47VgbUm2XNVXiwg.txt"}
}}